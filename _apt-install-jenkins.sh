#! /bin/bash
echo "************************  _app-install-jenkins ****************************"
apt-get install -y jenkins
if [ -n "$SET_JENKINS_HEAP_SIZE" ]; then
# Mejorar esto, prevenir agregar la linea varias veces
  echo -e "\nJAVA_ARGS=\"\$JAVA_ARGS -Xmx${SET_JENKINS_HEAP_SIZE}\"\n" >> /etc/default/jenkins
  systemctl restart jenkins
fi
