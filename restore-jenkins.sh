ORIGIN_WORKDIR=$(pwd)
WORKDIR=$(su jenkins -c "echo ~")
systemctl stop jenkins
cd $WORKDIR
su jenkins -c "git init"
su jenkins -c "git clean -df"
su jenkins -c "git remote add origin \"${JENKINS_RESTORE_ORIGIN}\""
su jenkins -c "git pull origin master"
systemctl start jenkins
cd $ORIGIN_WORKDIR
