#! /bin/bash
echo "************************  _apt-install-docker ****************************"
apt-get install -y docker-ce
if [ -n "$ADD_DOCKER_USER" ]; then
  echo "Adding docker user ${ADD_DOCKER_USER}"
  usermod -aG docker "$ADD_DOCKER_USER"
fi
