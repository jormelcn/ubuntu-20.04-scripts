#! /bin/bash
echo "************************  _add-jenkins-repository ****************************"
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y wget apt-transport-https ca-certificates software-properties-common
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update
